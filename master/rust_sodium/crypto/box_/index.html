<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `box_` mod in crate `rust_sodium`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, box_">

    <title>rust_sodium::crypto::box_ - Rust</title>

    <link rel="stylesheet" type="text/css" href="../../../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../../../main.css">
    

    <link rel="shortcut icon" href="https://maidsafe.net/img/favicon.ico">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../../../rust_sodium/index.html'><img src='https://raw.githubusercontent.com/maidsafe/QA/master/Images/maidsafe_logo.png' alt='logo' width='100'></a>
        <p class='location'><a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a></p><script>window.sidebarCurrent = {name: 'box_', ty: 'mod', relpath: '../'};</script><script defer src="../sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content mod">
<h1 class='fqn'><span class='in-band'>Module <a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a>::<wbr><a class='mod' href=''>box_</a></span><span class='out-of-band'><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a id='src-51' class='srclink' href='../../../src/rust_sodium/crypto/box_/mod.rs.html#1-78' title='goto source code'>[src]</a></span></h1>
<div class='docblock'><p>Public-key authenticated encryption</p>

<h1 id='security-model' class='section-header'><a href='#security-model'>Security model</a></h1>
<p>The <code>seal()</code> function is designed to meet the standard notions of privacy and
third-party unforgeability for a public-key authenticated-encryption scheme
using nonces. For formal definitions see, e.g., Jee Hea An, &quot;Authenticated
encryption in the public-key setting: security notions and analyses,&quot;
<a href="http://eprint.iacr.org/2001/079">http://eprint.iacr.org/2001/079</a>.</p>

<p>Distinct messages between the same {sender, receiver} set are required
to have distinct nonces. For example, the lexicographically smaller
public key can use nonce 1 for its first message to the other key, nonce
3 for its second message, nonce 5 for its third message, etc., while the
lexicographically larger public key uses nonce 2 for its first message
to the other key, nonce 4 for its second message, nonce 6 for its third
message, etc. Nonces are long enough that randomly generated nonces have
negligible risk of collision.</p>

<p>There is no harm in having the same nonce for different messages if the
{sender, receiver} sets are different. This is true even if the sets
overlap. For example, a sender can use the same nonce for two different
messages if the messages are sent to two different public keys.</p>

<p>The <code>seal()</code> function is not meant to provide non-repudiation. On the
contrary: the <code>seal()</code> function guarantees repudiability. A receiver
can freely modify a boxed message, and therefore cannot convince third
parties that this particular message came from the sender. The sender
and receiver are nevertheless protected against forgeries by other
parties. In the terminology of
<a href="http://groups.google.com/group/sci.crypt/msg/ec5c18b23b11d82c">http://groups.google.com/group/sci.crypt/msg/ec5c18b23b11d82c</a>,
<code>crypto_box</code> uses &quot;public-key authenticators&quot; rather than &quot;public-key
signatures.&quot;</p>

<p>Users who want public verifiability (or receiver-assisted public
verifiability) should instead use signatures (or signcryption).
Signature support is a high priority for <code>NaCl</code>; a signature API will be
described in subsequent <code>NaCl</code> documentation.</p>

<h1 id='selected-primitive' class='section-header'><a href='#selected-primitive'>Selected primitive</a></h1>
<p><code>seal()</code> is <code>crypto_box_curve25519xsalsa20poly1305</code> , a particular
combination of Curve25519, Salsa20, and Poly1305 specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>.</p>

<p>This function is conjectured to meet the standard notions of privacy and
third-party unforgeability.</p>

<h1 id='example-simple-interface' class='section-header'><a href='#example-simple-interface'>Example (simple interface)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>box_</span>;

<span class='kw'>let</span> (<span class='ident'>ourpk</span>, <span class='ident'>oursk</span>) <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>gen_keypair</span>();
<span class='comment'>// normally theirpk is sent by the other party</span>
<span class='kw'>let</span> (<span class='ident'>theirpk</span>, <span class='ident'>theirsk</span>) <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>gen_keypair</span>();
<span class='kw'>let</span> <span class='ident'>nonce</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>gen_nonce</span>();
<span class='kw'>let</span> <span class='ident'>plaintext</span> <span class='op'>=</span> <span class='string'>b&quot;some data&quot;</span>;
<span class='kw'>let</span> <span class='ident'>ciphertext</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>seal</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>theirpk</span>, <span class='kw-2'>&amp;</span><span class='ident'>oursk</span>);
<span class='kw'>let</span> <span class='ident'>their_plaintext</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>open</span>(<span class='kw-2'>&amp;</span><span class='ident'>ciphertext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>ourpk</span>, <span class='kw-2'>&amp;</span><span class='ident'>theirsk</span>).<span class='ident'>unwrap</span>();
<span class='macro'>assert</span><span class='macro'>!</span>(<span class='ident'>plaintext</span> <span class='op'>==</span> <span class='kw-2'>&amp;</span><span class='ident'>their_plaintext</span>[..]);</pre>

<h1 id='example-precomputation-interface' class='section-header'><a href='#example-precomputation-interface'>Example (precomputation interface)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>box_</span>;

<span class='kw'>let</span> (<span class='ident'>ourpk</span>, <span class='ident'>oursk</span>) <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>gen_keypair</span>();
<span class='kw'>let</span> (<span class='ident'>theirpk</span>, <span class='ident'>theirsk</span>) <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>gen_keypair</span>();
<span class='kw'>let</span> <span class='ident'>our_precomputed_key</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>precompute</span>(<span class='kw-2'>&amp;</span><span class='ident'>theirpk</span>, <span class='kw-2'>&amp;</span><span class='ident'>oursk</span>);
<span class='kw'>let</span> <span class='ident'>nonce</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>gen_nonce</span>();
<span class='kw'>let</span> <span class='ident'>plaintext</span> <span class='op'>=</span> <span class='string'>b&quot;plaintext&quot;</span>;
<span class='kw'>let</span> <span class='ident'>ciphertext</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>seal_precomputed</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>our_precomputed_key</span>);
<span class='comment'>// this will be identical to our_precomputed_key</span>
<span class='kw'>let</span> <span class='ident'>their_precomputed_key</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>precompute</span>(<span class='kw-2'>&amp;</span><span class='ident'>ourpk</span>, <span class='kw-2'>&amp;</span><span class='ident'>theirsk</span>);
<span class='kw'>let</span> <span class='ident'>their_plaintext</span> <span class='op'>=</span> <span class='ident'>box_</span>::<span class='ident'>open_precomputed</span>(<span class='kw-2'>&amp;</span><span class='ident'>ciphertext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>,
                                             <span class='kw-2'>&amp;</span><span class='ident'>their_precomputed_key</span>).<span class='ident'>unwrap</span>();
<span class='macro'>assert</span><span class='macro'>!</span>(<span class='ident'>plaintext</span> <span class='op'>==</span> <span class='kw-2'>&amp;</span><span class='ident'>their_plaintext</span>[..]);</pre>
</div><h2 id='reexports' class='section-header'><a href="#reexports">Reexports</a></h2>
<table><tr><td><code>pub use self::<a class='mod' href='../../../rust_sodium/crypto/box_/curve25519xsalsa20poly1305/index.html' title='rust_sodium::crypto::box_::curve25519xsalsa20poly1305'>curve25519xsalsa20poly1305</a>::*;</code></td></tr></table><h2 id='modules' class='section-header'><a href="#modules">Modules</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='mod' href='curve25519xsalsa20poly1305/index.html'
                                  title='rust_sodium::crypto::box_::curve25519xsalsa20poly1305'>curve25519xsalsa20poly1305</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_box_curve25519xsalsa20poly1305</code> , a particular
combination of Curve25519, Salsa20, and Poly1305 specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>.</p>
                           </td>
                       </tr></table></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../../../";
        window.currentCrate = "rust_sodium";
        window.playgroundUrl = "";
    </script>
    <script src="../../../jquery.js"></script>
    <script src="../../../main.js"></script>
    
    <script defer src="../../../search-index.js"></script>
</body>
</html>