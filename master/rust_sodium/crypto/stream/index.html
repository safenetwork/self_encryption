<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `stream` mod in crate `rust_sodium`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, stream">

    <title>rust_sodium::crypto::stream - Rust</title>

    <link rel="stylesheet" type="text/css" href="../../../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../../../main.css">
    

    <link rel="shortcut icon" href="https://maidsafe.net/img/favicon.ico">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../../../rust_sodium/index.html'><img src='https://raw.githubusercontent.com/maidsafe/QA/master/Images/maidsafe_logo.png' alt='logo' width='100'></a>
        <p class='location'><a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a></p><script>window.sidebarCurrent = {name: 'stream', ty: 'mod', relpath: '../'};</script><script defer src="../sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content mod">
<h1 class='fqn'><span class='in-band'>Module <a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a>::<wbr><a class='mod' href=''>stream</a></span><span class='out-of-band'><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a id='src-3000' class='srclink' href='../../../src/rust_sodium/crypto/stream/mod.rs.html#1-97' title='goto source code'>[src]</a></span></h1>
<div class='docblock'><p>Secret-key encryption</p>

<h1 id='security-model' class='section-header'><a href='#security-model'>Security Model</a></h1>
<p>The <code>stream()</code> function, viewed as a function of the nonce for a
uniform random key, is designed to meet the standard notion of
unpredictability (&quot;PRF&quot;). For a formal definition see, e.g., Section 2.3
of Bellare, Kilian, and Rogaway, &quot;The security of the cipher block
chaining message authentication code,&quot; Journal of Computer and System
Sciences 61 (2000), 362–399;
<a href="http://www-cse.ucsd.edu/%7Emihir/papers/cbc.html">http://www-cse.ucsd.edu/~mihir/papers/cbc.html</a>.</p>

<p>This means that an attacker cannot distinguish this function from a
uniform random function. Consequently, if a series of messages is
encrypted by <code>stream_xor()</code> with a different nonce for each message,
the ciphertexts are indistinguishable from uniform random strings of the
same length.</p>

<p>Note that the length is not hidden. Note also that it is the caller&#39;s
responsibility to ensure the uniqueness of nonces—for example, by using
nonce 1 for the first message, nonce 2 for the second message, etc.
Nonces are long enough that randomly generated nonces have negligible
risk of collision.</p>

<p><code>NaCl</code> does not make any promises regarding the resistance of <code>stream()</code> to
&quot;related-key attacks.&quot; It is the caller&#39;s responsibility to use proper
key-derivation functions.</p>

<h1 id='selected-primitive' class='section-header'><a href='#selected-primitive'>Selected primitive</a></h1>
<p><code>stream()</code> is <code>crypto_stream_xsalsa20</code>, a particular cipher specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>, Section 7.
This cipher is conjectured to meet the standard notion of
unpredictability.</p>

<h1 id='alternate-primitives' class='section-header'><a href='#alternate-primitives'>Alternate primitives</a></h1>
<p><code>NaCl</code> supports the following secret-key encryption functions:</p>

<hr>

<table>
<thead>
<tr>
<th><code>crypto_stream</code></th>
<th>primitive</th>
<th>KEYBYTES</th>
<th>NONCEBYTES</th>
</tr>
</thead>

<tbody>
<tr>
<td><code>crypto_stream_aes128ctr</code></td>
<td><code>AES-128-CTR</code></td>
<td>16</td>
<td>16</td>
</tr>
<tr>
<td><code>crypto_stream_chacha20</code></td>
<td><code>Chacha20/20</code></td>
<td>32</td>
<td>8</td>
</tr>
<tr>
<td><code>crypto_stream_salsa208</code></td>
<td><code>Salsa20/8</code></td>
<td>32</td>
<td>8</td>
</tr>
<tr>
<td><code>crypto_stream_salsa2012</code></td>
<td><code>Salsa20/12</code></td>
<td>32</td>
<td>8</td>
</tr>
<tr>
<td><code>crypto_stream_salsa20</code></td>
<td><code>Salsa20/20</code></td>
<td>32</td>
<td>8</td>
</tr>
<tr>
<td><code>crypto_stream_xsalsa20</code></td>
<td><code>XSalsa20/20</code></td>
<td>32</td>
<td>24</td>
</tr>
</tbody>
</table>

<hr>

<p>Beware that several of these primitives have 8-byte nonces. For those
primitives it is no longer true that randomly generated nonces have negligible
risk of collision. Callers who are unable to count 1, 2, 3..., and who insist
on using these primitives, are advised to use a randomly derived key for each
message.</p>

<h1 id='example-keystream-generation' class='section-header'><a href='#example-keystream-generation'>Example (keystream generation)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>stream</span>;

<span class='kw'>let</span> <span class='ident'>key</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>gen_key</span>();
<span class='kw'>let</span> <span class='ident'>nonce</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>gen_nonce</span>();
<span class='kw'>let</span> <span class='ident'>keystream</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>stream</span>(<span class='number'>128</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>); <span class='comment'>// generate 128 bytes of keystream</span></pre>

<h1 id='example-encryption' class='section-header'><a href='#example-encryption'>Example (encryption)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>stream</span>;

<span class='kw'>let</span> <span class='ident'>key</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>gen_key</span>();
<span class='kw'>let</span> <span class='ident'>nonce</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>gen_nonce</span>();
<span class='kw'>let</span> <span class='ident'>plaintext</span> <span class='op'>=</span> <span class='string'>b&quot;some data&quot;</span>;
<span class='kw'>let</span> <span class='ident'>ciphertext</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>stream_xor</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='kw'>let</span> <span class='ident'>their_plaintext</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>stream_xor</span>(<span class='kw-2'>&amp;</span><span class='ident'>ciphertext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='macro'>assert_eq</span><span class='macro'>!</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>their_plaintext</span>[..]);</pre>

<h1 id='example-in-place-encryption' class='section-header'><a href='#example-in-place-encryption'>Example (in place encryption)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>stream</span>;

<span class='kw'>let</span> <span class='ident'>key</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>gen_key</span>();
<span class='kw'>let</span> <span class='ident'>nonce</span> <span class='op'>=</span> <span class='ident'>stream</span>::<span class='ident'>gen_nonce</span>();
<span class='kw'>let</span> <span class='ident'>plaintext</span> <span class='op'>=</span> <span class='kw-2'>&amp;</span><span class='kw-2'>mut</span> [<span class='number'>0</span>, <span class='number'>1</span>, <span class='number'>2</span>, <span class='number'>3</span>];
<span class='comment'>// encrypt the plaintext</span>
<span class='ident'>stream</span>::<span class='ident'>stream_xor_inplace</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='comment'>// decrypt the plaintext</span>
<span class='ident'>stream</span>::<span class='ident'>stream_xor_inplace</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='macro'>assert_eq</span><span class='macro'>!</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='kw-2'>mut</span> [<span class='number'>0</span>, <span class='number'>1</span>, <span class='number'>2</span>, <span class='number'>3</span>]);</pre>
</div><h2 id='reexports' class='section-header'><a href="#reexports">Reexports</a></h2>
<table><tr><td><code>pub use self::<a class='mod' href='../../../rust_sodium/crypto/stream/xsalsa20/index.html' title='rust_sodium::crypto::stream::xsalsa20'>xsalsa20</a>::*;</code></td></tr></table><h2 id='modules' class='section-header'><a href="#modules">Modules</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='mod' href='aes128ctr/index.html'
                                  title='rust_sodium::crypto::stream::aes128ctr'>aes128ctr</a></td>
                           <td class='docblock short'>
                                <p><code>AES 128</code> in <code>CTR</code>-mode
This cipher is conjectured to meet the standard notion of
unpredictability.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='chacha20/index.html'
                                  title='rust_sodium::crypto::stream::chacha20'>chacha20</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_stream_chacha20</code> (Chacha20)</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='salsa20/index.html'
                                  title='rust_sodium::crypto::stream::salsa20'>salsa20</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_stream_salsa20</code> (Salsa20/20), a particular cipher specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>, Section 7.  This
cipher is conjectured to meet the standard notion of unpredictability.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='salsa2012/index.html'
                                  title='rust_sodium::crypto::stream::salsa2012'>salsa2012</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_stream_salsa2012</code> (Salsa20/12), a particular cipher specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>, Section 7.  This
cipher is conjectured to meet the standard notion of unpredictability.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='salsa208/index.html'
                                  title='rust_sodium::crypto::stream::salsa208'>salsa208</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_stream_salsa208</code> (Salsa20/8), a particular cipher specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>, Section 7.  This
cipher is conjectured to meet the standard notion of unpredictability.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='xsalsa20/index.html'
                                  title='rust_sodium::crypto::stream::xsalsa20'>xsalsa20</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_stream_xsalsa20</code>, a particular cipher specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>, Section 7.
This cipher is conjectured to meet the standard notion of
unpredictability.</p>
                           </td>
                       </tr></table></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../../../";
        window.currentCrate = "rust_sodium";
        window.playgroundUrl = "";
    </script>
    <script src="../../../jquery.js"></script>
    <script src="../../../main.js"></script>
    
    <script defer src="../../../search-index.js"></script>
</body>
</html>