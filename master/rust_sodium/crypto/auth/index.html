<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `auth` mod in crate `rust_sodium`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, auth">

    <title>rust_sodium::crypto::auth - Rust</title>

    <link rel="stylesheet" type="text/css" href="../../../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../../../main.css">
    

    <link rel="shortcut icon" href="https://maidsafe.net/img/favicon.ico">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../../../rust_sodium/index.html'><img src='https://raw.githubusercontent.com/maidsafe/QA/master/Images/maidsafe_logo.png' alt='logo' width='100'></a>
        <p class='location'><a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a></p><script>window.sidebarCurrent = {name: 'auth', ty: 'mod', relpath: '../'};</script><script defer src="../sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content mod">
<h1 class='fqn'><span class='in-band'>Module <a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a>::<wbr><a class='mod' href=''>auth</a></span><span class='out-of-band'><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a id='src-1223' class='srclink' href='../../../src/rust_sodium/crypto/auth/mod.rs.html#1-74' title='goto source code'>[src]</a></span></h1>
<div class='docblock'><p>Secret-key authentication</p>

<h1 id='security-model' class='section-header'><a href='#security-model'>Security model</a></h1>
<p>The <code>authenticate()</code> function, viewed as a function of the
message for a uniform random key, is designed to meet the standard
notion of unforgeability. This means that an attacker cannot find
authenticators for any messages not authenticated by the sender, even if
the attacker has adaptively influenced the messages authenticated by the
sender. For a formal definition see, e.g., Section 2.4 of Bellare,
Kilian, and Rogaway, &quot;The security of the cipher block chaining message
authentication code,&quot; Journal of Computer and System Sciences 61 (2000),
362–399; <a href="http://www-cse.ucsd.edu/%7Emihir/papers/cbc.html">http://www-cse.ucsd.edu/~mihir/papers/cbc.html</a>.</p>

<p><code>NaCl</code> does not make any promises regarding &quot;strong&quot; unforgeability;
perhaps one valid authenticator can be converted into another valid
authenticator for the same message. <code>NaCl</code> also does not make any promises
regarding &quot;truncated unforgeability.&quot;</p>

<h1 id='selected-primitive' class='section-header'><a href='#selected-primitive'>Selected primitive</a></h1>
<p><code>authenticate()</code> is currently an implementation of
<code>HMAC-SHA-512-256</code>, i.e., the first 256 bits of <code>HMAC-SHA-512</code>.
<code>HMAC-SHA-512-256</code> is conjectured to meet the standard notion of
unforgeability.</p>

<h1 id='alternate-primitives' class='section-header'><a href='#alternate-primitives'>Alternate primitives</a></h1>
<p><code>NaCl</code> supports the following secret-key authentication functions:</p>

<hr>

<table>
<thead>
<tr>
<th><code>crypto_auth</code></th>
<th>primitive</th>
<th>BYTES</th>
<th>KEYBYTES</th>
</tr>
</thead>

<tbody>
<tr>
<td><code>crypto_auth_hmacsha256</code></td>
<td><code>HMAC_SHA-256</code></td>
<td>32</td>
<td>32</td>
</tr>
<tr>
<td><code>crypto_auth_hmacsha512256</code></td>
<td><code>HMAC_SHA-512-256</code></td>
<td>32</td>
<td>32</td>
</tr>
<tr>
<td><code>crypto_auth_hmacsha512</code></td>
<td><code>HMAC_SHA-512</code></td>
<td>64</td>
<td>32</td>
</tr>
</tbody>
</table>

<hr>

<h1 id='example-simple-interface' class='section-header'><a href='#example-simple-interface'>Example (simple interface)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>auth</span>;

<span class='kw'>let</span> <span class='ident'>key</span> <span class='op'>=</span> <span class='ident'>auth</span>::<span class='ident'>gen_key</span>();
<span class='kw'>let</span> <span class='ident'>data_to_authenticate</span> <span class='op'>=</span> <span class='string'>b&quot;some data&quot;</span>;
<span class='kw'>let</span> <span class='ident'>tag</span> <span class='op'>=</span> <span class='ident'>auth</span>::<span class='ident'>authenticate</span>(<span class='ident'>data_to_authenticate</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='macro'>assert</span><span class='macro'>!</span>(<span class='ident'>auth</span>::<span class='ident'>verify</span>(<span class='kw-2'>&amp;</span><span class='ident'>tag</span>, <span class='ident'>data_to_authenticate</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>));</pre>

<h1 id='example-streaming-interface' class='section-header'><a href='#example-streaming-interface'>Example (streaming interface)</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>auth</span>;
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>randombytes</span>;

<span class='kw'>let</span> <span class='ident'>key</span> <span class='op'>=</span> <span class='ident'>randombytes</span>::<span class='ident'>randombytes</span>(<span class='number'>123</span>);

<span class='kw'>let</span> <span class='ident'>data_part_1</span> <span class='op'>=</span> <span class='string'>b&quot;some data&quot;</span>;
<span class='kw'>let</span> <span class='ident'>data_part_2</span> <span class='op'>=</span> <span class='string'>b&quot;some other data&quot;</span>;
<span class='kw'>let</span> <span class='kw-2'>mut</span> <span class='ident'>state</span> <span class='op'>=</span> <span class='ident'>auth</span>::<span class='ident'>State</span>::<span class='ident'>init</span>(<span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='ident'>state</span>.<span class='ident'>update</span>(<span class='ident'>data_part_1</span>);
<span class='ident'>state</span>.<span class='ident'>update</span>(<span class='ident'>data_part_2</span>);
<span class='kw'>let</span> <span class='ident'>tag1</span> <span class='op'>=</span> <span class='ident'>state</span>.<span class='ident'>finalize</span>();

<span class='kw'>let</span> <span class='ident'>data_2_part_1</span> <span class='op'>=</span> <span class='string'>b&quot;some datasome &quot;</span>;
<span class='kw'>let</span> <span class='ident'>data_2_part_2</span> <span class='op'>=</span> <span class='string'>b&quot;other data&quot;</span>;
<span class='kw'>let</span> <span class='kw-2'>mut</span> <span class='ident'>state</span> <span class='op'>=</span> <span class='ident'>auth</span>::<span class='ident'>State</span>::<span class='ident'>init</span>(<span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='ident'>state</span>.<span class='ident'>update</span>(<span class='ident'>data_2_part_1</span>);
<span class='ident'>state</span>.<span class='ident'>update</span>(<span class='ident'>data_2_part_2</span>);
<span class='kw'>let</span> <span class='ident'>tag2</span> <span class='op'>=</span> <span class='ident'>state</span>.<span class='ident'>finalize</span>();
<span class='macro'>assert_eq</span><span class='macro'>!</span>(<span class='ident'>tag1</span>, <span class='ident'>tag2</span>);</pre>
</div><h2 id='reexports' class='section-header'><a href="#reexports">Reexports</a></h2>
<table><tr><td><code>pub use self::<a class='mod' href='../../../rust_sodium/crypto/auth/hmacsha512256/index.html' title='rust_sodium::crypto::auth::hmacsha512256'>hmacsha512256</a>::*;</code></td></tr></table><h2 id='modules' class='section-header'><a href="#modules">Modules</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='mod' href='hmacsha256/index.html'
                                  title='rust_sodium::crypto::auth::hmacsha256'>hmacsha256</a></td>
                           <td class='docblock short'>
                                <p><code>HMAC-SHA-256</code> <code>HMAC-SHA-256</code> is conjectured to meet the standard notion of
unforgeability.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='hmacsha512/index.html'
                                  title='rust_sodium::crypto::auth::hmacsha512'>hmacsha512</a></td>
                           <td class='docblock short'>
                                <p><code>HMAC-SHA-512</code> <code>HMAC-SHA-512</code> is conjectured to meet the standard notion of
unforgeability.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='mod' href='hmacsha512256/index.html'
                                  title='rust_sodium::crypto::auth::hmacsha512256'>hmacsha512256</a></td>
                           <td class='docblock short'>
                                <p><code>HMAC-SHA-512-256</code>, i.e., the first 256 bits of
<code>HMAC-SHA-512</code>.  <code>HMAC-SHA-512-256</code> is conjectured to meet the standard notion
of unforgeability.</p>
                           </td>
                       </tr></table></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../../../";
        window.currentCrate = "rust_sodium";
        window.playgroundUrl = "";
    </script>
    <script src="../../../jquery.js"></script>
    <script src="../../../main.js"></script>
    
    <script defer src="../../../search-index.js"></script>
</body>
</html>