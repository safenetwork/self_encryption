<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `secretbox` mod in crate `rust_sodium`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, secretbox">

    <title>rust_sodium::crypto::secretbox - Rust</title>

    <link rel="stylesheet" type="text/css" href="../../../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../../../main.css">
    

    <link rel="shortcut icon" href="https://maidsafe.net/img/favicon.ico">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../../../rust_sodium/index.html'><img src='https://raw.githubusercontent.com/maidsafe/QA/master/Images/maidsafe_logo.png' alt='logo' width='100'></a>
        <p class='location'><a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a></p><script>window.sidebarCurrent = {name: 'secretbox', ty: 'mod', relpath: '../'};</script><script defer src="../sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content mod">
<h1 class='fqn'><span class='in-band'>Module <a href='../../index.html'>rust_sodium</a>::<wbr><a href='../index.html'>crypto</a>::<wbr><a class='mod' href=''>secretbox</a></span><span class='out-of-band'><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a id='src-2240' class='srclink' href='../../../src/rust_sodium/crypto/secretbox/mod.rs.html#1-37' title='goto source code'>[src]</a></span></h1>
<div class='docblock'><p>Secret-key authenticated encryption</p>

<h1 id='security-model' class='section-header'><a href='#security-model'>Security model</a></h1>
<p>The <code>seal()</code> function is designed to meet the standard notions of privacy and
authenticity for a secret-key authenticated-encryption scheme using nonces. For
formal definitions see, e.g., Bellare and Namprempre, &quot;Authenticated
encryption: relations among notions and analysis of the generic composition
paradigm,&quot; Lecture Notes in Computer Science 1976 (2000), 531–545,
<a href="http://www-cse.ucsd.edu/%7Emihir/papers/oem.html">http://www-cse.ucsd.edu/~mihir/papers/oem.html</a>.</p>

<p>Note that the length is not hidden. Note also that it is the caller&#39;s
responsibility to ensure the uniqueness of nonces—for example, by using
nonce 1 for the first message, nonce 2 for the second message, etc.
Nonces are long enough that randomly generated nonces have negligible
risk of collision.</p>

<h1 id='selected-primitive' class='section-header'><a href='#selected-primitive'>Selected primitive</a></h1>
<p><code>seal()</code> is <code>crypto_secretbox_xsalsa20poly1305</code>, a particular
combination of Salsa20 and Poly1305 specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>.</p>

<p>This function is conjectured to meet the standard notions of privacy and
authenticity.</p>

<h1 id='example' class='section-header'><a href='#example'>Example</a></h1>
<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>rust_sodium</span>::<span class='ident'>crypto</span>::<span class='ident'>secretbox</span>;
<span class='kw'>let</span> <span class='ident'>key</span> <span class='op'>=</span> <span class='ident'>secretbox</span>::<span class='ident'>gen_key</span>();
<span class='kw'>let</span> <span class='ident'>nonce</span> <span class='op'>=</span> <span class='ident'>secretbox</span>::<span class='ident'>gen_nonce</span>();
<span class='kw'>let</span> <span class='ident'>plaintext</span> <span class='op'>=</span> <span class='string'>b&quot;some data&quot;</span>;
<span class='kw'>let</span> <span class='ident'>ciphertext</span> <span class='op'>=</span> <span class='ident'>secretbox</span>::<span class='ident'>seal</span>(<span class='ident'>plaintext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>);
<span class='kw'>let</span> <span class='ident'>their_plaintext</span> <span class='op'>=</span> <span class='ident'>secretbox</span>::<span class='ident'>open</span>(<span class='kw-2'>&amp;</span><span class='ident'>ciphertext</span>, <span class='kw-2'>&amp;</span><span class='ident'>nonce</span>, <span class='kw-2'>&amp;</span><span class='ident'>key</span>).<span class='ident'>unwrap</span>();
<span class='macro'>assert</span><span class='macro'>!</span>(<span class='ident'>plaintext</span> <span class='op'>==</span> <span class='kw-2'>&amp;</span><span class='ident'>their_plaintext</span>[..]);</pre>
</div><h2 id='reexports' class='section-header'><a href="#reexports">Reexports</a></h2>
<table><tr><td><code>pub use self::<a class='mod' href='../../../rust_sodium/crypto/secretbox/xsalsa20poly1305/index.html' title='rust_sodium::crypto::secretbox::xsalsa20poly1305'>xsalsa20poly1305</a>::*;</code></td></tr></table><h2 id='modules' class='section-header'><a href="#modules">Modules</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='mod' href='xsalsa20poly1305/index.html'
                                  title='rust_sodium::crypto::secretbox::xsalsa20poly1305'>xsalsa20poly1305</a></td>
                           <td class='docblock short'>
                                <p><code>crypto_secretbox_xsalsa20poly1305</code>, a particular
combination of Salsa20 and Poly1305 specified in
<a href="http://nacl.cr.yp.to/valid.html">Cryptography in <code>NaCl</code></a>.</p>
                           </td>
                       </tr></table></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../../../";
        window.currentCrate = "rust_sodium";
        window.playgroundUrl = "";
    </script>
    <script src="../../../jquery.js"></script>
    <script src="../../../main.js"></script>
    
    <script defer src="../../../search-index.js"></script>
</body>
</html>