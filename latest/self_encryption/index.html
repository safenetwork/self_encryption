<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `self_encryption` crate.">
    <meta name="keywords" content="rust, rustlang, rust-lang, self_encryption">

    <title>self_encryption - Rust</title>

    <link rel="stylesheet" type="text/css" href="../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../main.css">
    

    <link rel="shortcut icon" href="http://maidsafe.net/img/favicon.ico">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../self_encryption/index.html'><img src='https://raw.githubusercontent.com/maidsafe/QA/master/Images/maidsafe_logo.png' alt='logo' width='100'></a>
        <p class='location'></p><script>window.sidebarCurrent = {name: 'self_encryption', ty: 'mod', relpath: '../'};</script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content mod">
<h1 class='fqn'><span class='in-band'>Crate <a class='mod' href=''>self_encryption</a></span><span class='out-of-band'><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a id='src-0' class='srclink' href='../src/self_encryption/src/lib.rs.html#18-180' title='goto source code'>[src]</a></span></h1>
<div class='docblock'><p>A file <strong>content</strong> self-encryptor.</p>

<p>This library provides convergent encryption on file-based data and produce a <code>DataMap</code> type and
several chunks of data. Each chunk is up to 1MB in size and has a name.  This name is the SHA512
hash of the content, which allows the chunks to be self-validating.  If size and hash checks are
utilised, a high degree of certainty in the validity of the data can be expected.</p>

<p><a href="https://github.com/maidsafe/self_encryption">Project GitHub page</a>.</p>

<h1 id='use' class='section-header'><a href='#use'>Use</a></h1>
<p>To use this library you must implement a storage trait (a key/value store) and associated
storage error trait.  These provide a place for encrypted chunks to be put to and got from by
the <code>SelfEncryptor</code>.</p>

<p>The storage trait should be flexible enough to allow implementation as an in-memory map, a
disk-based database, or a network-based DHT for example.</p>

<h1 id='examples' class='section-header'><a href='#examples'>Examples</a></h1>
<p>This is a simple setup for a memory-based chunk store.  A working implementation can be found
in the &quot;examples&quot; folder of this project.</p>

<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>std</span>::<span class='ident'>error</span>::<span class='ident'>Error</span>;
<span class='kw'>use</span> <span class='ident'>std</span>::<span class='ident'>fmt</span>::{<span class='self'>self</span>, <span class='ident'>Display</span>, <span class='ident'>Formatter</span>};
<span class='kw'>use</span> <span class='ident'>self_encryption</span>::{<span class='ident'>Storage</span>, <span class='ident'>StorageError</span>};

<span class='attribute'>#[<span class='ident'>derive</span>(<span class='ident'>Debug</span>, <span class='ident'>Clone</span>)]</span>
<span class='kw'>struct</span> <span class='ident'>SimpleStorageError</span> {}

<span class='kw'>impl</span> <span class='ident'>Display</span> <span class='kw'>for</span> <span class='ident'>SimpleStorageError</span> {
   <span class='kw'>fn</span> <span class='ident'>fmt</span>(<span class='kw-2'>&amp;</span><span class='self'>self</span>, <span class='ident'>formatter</span>: <span class='kw-2'>&amp;</span><span class='kw-2'>mut</span> <span class='ident'>Formatter</span>) <span class='op'>-&gt;</span> <span class='ident'>fmt</span>::<span class='prelude-ty'>Result</span> {
       <span class='macro'>write</span><span class='macro'>!</span>(<span class='ident'>formatter</span>, <span class='string'>&quot;Failed to get data from SimpleStorage&quot;</span>)
   }
}

<span class='kw'>impl</span> <span class='ident'>Error</span> <span class='kw'>for</span> <span class='ident'>SimpleStorageError</span> {
    <span class='kw'>fn</span> <span class='ident'>description</span>(<span class='kw-2'>&amp;</span><span class='self'>self</span>) <span class='op'>-&gt;</span> <span class='kw-2'>&amp;</span><span class='ident'>str</span> {
        <span class='string'>&quot;SimpleStorage::get() error&quot;</span>
    }
}

<span class='kw'>impl</span> <span class='ident'>StorageError</span> <span class='kw'>for</span> <span class='ident'>SimpleStorageError</span> {}

<span class='kw'>struct</span> <span class='ident'>Entry</span> {
    <span class='ident'>name</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>u8</span><span class='op'>&gt;</span>,
    <span class='ident'>data</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>u8</span><span class='op'>&gt;</span>
}

<span class='kw'>struct</span> <span class='ident'>SimpleStorage</span> {
    <span class='ident'>entries</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>Entry</span><span class='op'>&gt;</span>
}

<span class='kw'>impl</span> <span class='ident'>SimpleStorage</span> {
    <span class='kw'>fn</span> <span class='ident'>new</span>() <span class='op'>-&gt;</span> <span class='ident'>SimpleStorage</span> {
        <span class='ident'>SimpleStorage</span> { <span class='ident'>entries</span>: <span class='macro'>vec</span><span class='macro'>!</span>[] }
    }
}

<span class='kw'>impl</span> <span class='ident'>Storage</span><span class='op'>&lt;</span><span class='ident'>SimpleStorageError</span><span class='op'>&gt;</span> <span class='kw'>for</span> <span class='ident'>SimpleStorage</span> {
   <span class='kw'>fn</span> <span class='ident'>get</span>(<span class='kw-2'>&amp;</span><span class='self'>self</span>, <span class='ident'>name</span>: <span class='kw-2'>&amp;</span>[<span class='ident'>u8</span>]) <span class='op'>-&gt;</span> <span class='prelude-ty'>Result</span><span class='op'>&lt;</span><span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>u8</span><span class='op'>&gt;</span>, <span class='ident'>SimpleStorageError</span><span class='op'>&gt;</span> {
       <span class='kw'>match</span> <span class='self'>self</span>.<span class='ident'>entries</span>.<span class='ident'>iter</span>().<span class='ident'>find</span>(<span class='op'>|</span><span class='kw-2'>ref</span> <span class='ident'>entry</span><span class='op'>|</span> <span class='ident'>entry</span>.<span class='ident'>name</span> <span class='op'>==</span> <span class='ident'>name</span>) {
           <span class='prelude-val'>Some</span>(<span class='ident'>entry</span>) <span class='op'>=&gt;</span> <span class='prelude-val'>Ok</span>(<span class='ident'>entry</span>.<span class='ident'>data</span>.<span class='ident'>clone</span>()),
           <span class='prelude-val'>None</span> <span class='op'>=&gt;</span> <span class='prelude-val'>Err</span>(<span class='ident'>SimpleStorageError</span> {}),
       }
   }

   <span class='kw'>fn</span> <span class='ident'>put</span>(<span class='kw-2'>&amp;</span><span class='kw-2'>mut</span> <span class='self'>self</span>, <span class='ident'>name</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>u8</span><span class='op'>&gt;</span>, <span class='ident'>data</span>: <span class='ident'>Vec</span><span class='op'>&lt;</span><span class='ident'>u8</span><span class='op'>&gt;</span>) <span class='op'>-&gt;</span> <span class='prelude-ty'>Result</span><span class='op'>&lt;</span>(), <span class='ident'>SimpleStorageError</span><span class='op'>&gt;</span> {
       <span class='prelude-val'>Ok</span>(<span class='self'>self</span>.<span class='ident'>entries</span>.<span class='ident'>push</span>(<span class='ident'>Entry</span> {
           <span class='ident'>name</span>: <span class='ident'>name</span>,
           <span class='ident'>data</span>: <span class='ident'>data</span>,
       }))
   }
}</pre>

<p>Using this <code>SimpleStorage</code>, a self-encryptor can be created and written to/read from:</p>

<pre class='rust rust-example-rendered'>
<span class='kw'>use</span> <span class='ident'>self_encryption</span>::{<span class='ident'>DataMap</span>, <span class='ident'>SelfEncryptor</span>};

<span class='kw'>let</span> <span class='kw-2'>mut</span> <span class='ident'>storage</span> <span class='op'>=</span> <span class='ident'>SimpleStorage</span>::<span class='ident'>new</span>();
<span class='kw'>let</span> <span class='kw-2'>mut</span> <span class='ident'>encryptor</span> <span class='op'>=</span> <span class='ident'>SelfEncryptor</span>::<span class='ident'>new</span>(<span class='kw-2'>&amp;</span><span class='kw-2'>mut</span> <span class='ident'>storage</span>, <span class='ident'>DataMap</span>::<span class='prelude-val'>None</span>).<span class='ident'>unwrap</span>();
<span class='kw'>let</span> <span class='ident'>data</span> <span class='op'>=</span> <span class='macro'>vec</span><span class='macro'>!</span>[<span class='number'>0</span>, <span class='number'>1</span>, <span class='number'>2</span>, <span class='number'>3</span>, <span class='number'>4</span>, <span class='number'>5</span>];
<span class='kw'>let</span> <span class='kw-2'>mut</span> <span class='ident'>offset</span> <span class='op'>=</span> <span class='number'>0</span>;

<span class='ident'>encryptor</span>.<span class='ident'>write</span>(<span class='kw-2'>&amp;</span><span class='ident'>data</span>, <span class='ident'>offset</span>).<span class='ident'>unwrap</span>();

<span class='ident'>offset</span> <span class='op'>=</span> <span class='number'>2</span>;
<span class='kw'>let</span> <span class='ident'>length</span> <span class='op'>=</span> <span class='number'>3</span>;
<span class='macro'>assert_eq</span><span class='macro'>!</span>(<span class='ident'>encryptor</span>.<span class='ident'>read</span>(<span class='ident'>offset</span>, <span class='ident'>length</span>).<span class='ident'>unwrap</span>(), <span class='macro'>vec</span><span class='macro'>!</span>[<span class='number'>2</span>, <span class='number'>3</span>, <span class='number'>4</span>]);

<span class='kw'>let</span> <span class='ident'>data_map</span> <span class='op'>=</span> <span class='ident'>encryptor</span>.<span class='ident'>close</span>().<span class='ident'>unwrap</span>();
<span class='macro'>assert_eq</span><span class='macro'>!</span>(<span class='ident'>data_map</span>.<span class='ident'>len</span>(), <span class='number'>6</span>);</pre>

<p>The <code>close()</code> function returns a <code>DataMap</code> which can be used when creating a new encryptor to
access the content previously written.  Storage of the <code>DataMap</code> is outwith the scope of this
library and must be implemented by the user.</p>
</div><h2 id='structs' class='section-header'><a href="#structs">Structs</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='struct' href='struct.ChunkDetails.html'
                                  title='self_encryption::ChunkDetails'>ChunkDetails</a></td>
                           <td class='docblock short'>
                                <p>Holds pre- and post-encryption hashes as well as the original (pre-compression) size for a given
chunk.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='struct' href='struct.SelfEncryptor.html'
                                  title='self_encryption::SelfEncryptor'>SelfEncryptor</a></td>
                           <td class='docblock short'>
                                <p>This is the encryption object and all file handling should be done using this object as the low
level mechanism to read and write <em>content</em>.  This library has no knowledge of file metadata.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='struct' href='struct.SequentialEncryptor.html'
                                  title='self_encryption::SequentialEncryptor'>SequentialEncryptor</a></td>
                           <td class='docblock short'>
                                <p>An encryptor which only permits sequential writes, i.e. there is no ability to specify an offset
in the <code>write()</code> call; all data is appended sequentially.</p>
                           </td>
                       </tr></table><h2 id='enums' class='section-header'><a href="#enums">Enums</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='enum' href='enum.DataMap.html'
                                  title='self_encryption::DataMap'>DataMap</a></td>
                           <td class='docblock short'>
                                <p>Holds the information that is required to recover the content of the encrypted file.  Depending
on the file size, this is held as a vector of <code>ChunkDetails</code>, or as raw data.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='enum' href='enum.SelfEncryptionError.html'
                                  title='self_encryption::SelfEncryptionError'>SelfEncryptionError</a></td>
                           <td class='docblock short'>
                                <p>Errors which can arise during self-encryption or -decryption.</p>
                           </td>
                       </tr></table><h2 id='constants' class='section-header'><a href="#constants">Constants</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='constant' href='constant.COMPRESSION_QUALITY.html'
                                  title='self_encryption::COMPRESSION_QUALITY'>COMPRESSION_QUALITY</a></td>
                           <td class='docblock short'>
                                <p>Controls the compression-speed vs compression-density tradeoffs.  The higher the quality, the
slower the compression.  Range is 0 to 11.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='constant' href='constant.MAX_CHUNK_SIZE.html'
                                  title='self_encryption::MAX_CHUNK_SIZE'>MAX_CHUNK_SIZE</a></td>
                           <td class='docblock short'>
                                <p>The maximum size (before compression) of an individual chunk of the file, defined as 1MB.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='constant' href='constant.MAX_FILE_SIZE.html'
                                  title='self_encryption::MAX_FILE_SIZE'>MAX_FILE_SIZE</a></td>
                           <td class='docblock short'>
                                <p>The maximum size of file which can be self-encrypted, defined as 1GB.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='constant' href='constant.MIN_CHUNK_SIZE.html'
                                  title='self_encryption::MIN_CHUNK_SIZE'>MIN_CHUNK_SIZE</a></td>
                           <td class='docblock short'>
                                <p>The minimum size (before compression) of an individual chunk of the file, defined as 1kB.</p>
                           </td>
                       </tr></table><h2 id='traits' class='section-header'><a href="#traits">Traits</a></h2>
<table>
                       <tr class=' module-item'>
                           <td><a class='trait' href='trait.Storage.html'
                                  title='self_encryption::Storage'>Storage</a></td>
                           <td class='docblock short'>
                                <p>Trait which must be implemented by storage objects to be used in self-encryption.  Data is
passed to the storage object encrypted with <code>name</code> being the SHA512 hash of <code>data</code>.  <code>Storage</code>
could be implemented as an in-memory <code>HashMap</code> or a disk-based container for example.</p>
                           </td>
                       </tr>
                       <tr class=' module-item'>
                           <td><a class='trait' href='trait.StorageError.html'
                                  title='self_encryption::StorageError'>StorageError</a></td>
                           <td class='docblock short'>
                                <p>Trait inherited from <code>std::error::Error</code> representing errors which can be returned by the
<code>Storage</code> object.</p>
                           </td>
                       </tr></table></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../";
        window.currentCrate = "self_encryption";
        window.playgroundUrl = "";
    </script>
    <script src="../jquery.js"></script>
    <script src="../main.js"></script>
    
    <script defer src="../search-index.js"></script>
</body>
</html>