<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="rustdoc">
    <meta name="description" content="API documentation for the Rust `SequentialEncryptor` struct in crate `self_encryption`.">
    <meta name="keywords" content="rust, rustlang, rust-lang, SequentialEncryptor">

    <title>self_encryption::SequentialEncryptor - Rust</title>

    <link rel="stylesheet" type="text/css" href="../rustdoc.css">
    <link rel="stylesheet" type="text/css" href="../main.css">
    

    <link rel="shortcut icon" href="http://maidsafe.net/img/favicon.ico">
    
</head>
<body class="rustdoc">
    <!--[if lte IE 8]>
    <div class="warning">
        This old browser is unsupported and will most likely display funky
        things.
    </div>
    <![endif]-->

    

    <nav class="sidebar">
        <a href='../self_encryption/index.html'><img src='https://raw.githubusercontent.com/maidsafe/QA/master/Images/maidsafe_logo.png' alt='logo' width='100'></a>
        <p class='location'><a href='index.html'>self_encryption</a></p><script>window.sidebarCurrent = {name: 'SequentialEncryptor', ty: 'struct', relpath: ''};</script><script defer src="sidebar-items.js"></script>
    </nav>

    <nav class="sub">
        <form class="search-form js-only">
            <div class="search-container">
                <input class="search-input" name="search"
                       autocomplete="off"
                       placeholder="Click or press ‘S’ to search, ‘?’ for more options…"
                       type="search">
            </div>
        </form>
    </nav>

    <section id='main' class="content struct">
<h1 class='fqn'><span class='in-band'>Struct <a href='index.html'>self_encryption</a>::<wbr><a class='struct' href=''>SequentialEncryptor</a></span><span class='out-of-band'><span id='render-detail'>
                   <a id="toggle-all-docs" href="javascript:void(0)" title="collapse all docs">
                       [<span class='inner'>&#x2212;</span>]
                   </a>
               </span><a id='src-965' class='srclink' href='../src/self_encryption/src/sequential/encryptor.rs.html#89-91' title='goto source code'>[src]</a></span></h1>
<pre class='rust struct'>pub struct SequentialEncryptor&lt;'a,&nbsp;E:&nbsp;<a class='trait' href='../self_encryption/trait.StorageError.html' title='self_encryption::StorageError'>StorageError</a>,&nbsp;S:&nbsp;'a + <a class='trait' href='../self_encryption/trait.Storage.html' title='self_encryption::Storage'>Storage</a>&lt;E&gt;&gt; {
    // some fields omitted
}</pre><div class='docblock'><p>An encryptor which only permits sequential writes, i.e. there is no ability to specify an offset
in the <code>write()</code> call; all data is appended sequentially.</p>

<p>The resulting chunks and <code>DataMap</code> are identical to those which would have been produced by a
<a href="struct.SelfEncryptor.html"><code>SelfEncryptor</code></a>.</p>

<p>This encryptor differs from <code>SelfEncryptor</code> in that completed chunks will be stored during
<code>write()</code> calls as opposed to buffering all data until the <code>close()</code> call.  This should give
more realistic feedback about the progress of fully self-encrypting larger data.</p>

<p>A further difference is that since the entire data is not held in an internal buffer, this
encryptor doesn&#39;t need to limit the input data size, i.e. <code>MAX_FILE_SIZE</code> does not apply to this
encryptor.  (Note that as of writing, there is no way to decrypt data which exceeds this size,
since the only decryptor available is <code>SelfEncryptor</code>, and this <em>does</em> limit the data size to
<code>MAX_FILE_SIZE</code>.)</p>

<p>Due to the reduced complexity, a side effect is that this encryptor outperforms <code>SelfEncryptor</code>,
particularly for small data (below <code>MIN_CHUNK_SIZE * 3</code> bytes) where no chunks are generated.</p>
</div><h2 id='methods'>Methods</h2><h3 class='impl'><span class='in-band'><code>impl&lt;'a,&nbsp;E:&nbsp;<a class='trait' href='../self_encryption/trait.StorageError.html' title='self_encryption::StorageError'>StorageError</a>,&nbsp;S:&nbsp;<a class='trait' href='../self_encryption/trait.Storage.html' title='self_encryption::Storage'>Storage</a>&lt;E&gt;&gt; <a class='struct' href='../self_encryption/struct.SequentialEncryptor.html' title='self_encryption::SequentialEncryptor'>Encryptor</a>&lt;'a,&nbsp;E,&nbsp;S&gt;</code></span><span class='out-of-band'><div class='ghost'></div><a id='src-970' class='srclink' href='../src/self_encryption/src/sequential/encryptor.rs.html#93-183' title='goto source code'>[src]</a></span></h3>
<div class='impl-items'><h4 id='method.new' class='method'><code>fn <a href='#method.new' class='fnname'>new</a>(storage: &amp;'a mut S, data_map: <a class='enum' href='https://doc.rust-lang.org/nightly/core/option/enum.Option.html' title='core::option::Option'>Option</a>&lt;<a class='enum' href='../self_encryption/enum.DataMap.html' title='self_encryption::DataMap'>DataMap</a>&gt;) -&gt; <a class='enum' href='https://doc.rust-lang.org/nightly/core/result/enum.Result.html' title='core::result::Result'>Result</a>&lt;<a class='struct' href='../self_encryption/struct.SequentialEncryptor.html' title='self_encryption::SequentialEncryptor'>Encryptor</a>&lt;'a,&nbsp;E,&nbsp;S&gt;,&nbsp;<a class='enum' href='../self_encryption/enum.SelfEncryptionError.html' title='self_encryption::SelfEncryptionError'>SelfEncryptionError</a>&lt;E&gt;&gt;</code></h4>
<div class='docblock'><p>Creates an <code>Encryptor</code>, using an existing <code>DataMap</code> if <code>data_map</code> is not <code>None</code>.</p>
</div><h4 id='method.write' class='method'><code>fn <a href='#method.write' class='fnname'>write</a>(&amp;mut self, data: <a class='primitive' href='https://doc.rust-lang.org/nightly/std/primitive.slice.html'>&amp;[</a><a class='primitive' href='https://doc.rust-lang.org/nightly/std/primitive.u8.html'>u8</a><a class='primitive' href='https://doc.rust-lang.org/nightly/std/primitive.slice.html'>]</a>) -&gt; <a class='enum' href='https://doc.rust-lang.org/nightly/core/result/enum.Result.html' title='core::result::Result'>Result</a>&lt;<a class='primitive' href='https://doc.rust-lang.org/nightly/std/primitive.tuple.html'>()</a>,&nbsp;<a class='enum' href='../self_encryption/enum.SelfEncryptionError.html' title='self_encryption::SelfEncryptionError'>SelfEncryptionError</a>&lt;E&gt;&gt;</code></h4>
<div class='docblock'><p>Buffers some or all of <code>data</code> and stores any completed chunks (i.e. those which cannot be
modified by subsequent <code>write()</code> calls).  The internal buffers can only be flushed by
calling <code>close()</code>.</p>
</div><h4 id='method.close' class='method'><code>fn <a href='#method.close' class='fnname'>close</a>(&amp;mut self) -&gt; <a class='enum' href='https://doc.rust-lang.org/nightly/core/result/enum.Result.html' title='core::result::Result'>Result</a>&lt;<a class='enum' href='../self_encryption/enum.DataMap.html' title='self_encryption::DataMap'>DataMap</a>,&nbsp;<a class='enum' href='../self_encryption/enum.SelfEncryptionError.html' title='self_encryption::SelfEncryptionError'>SelfEncryptionError</a>&lt;E&gt;&gt;</code></h4>
<div class='docblock'><p>This finalises the encryptor - it should not be used again after this call.  Internal
buffers are flushed, resulting in up to four chunks being stored.</p>
</div><h4 id='method.len' class='method'><code>fn <a href='#method.len' class='fnname'>len</a>(&amp;self) -&gt; <a class='primitive' href='https://doc.rust-lang.org/nightly/std/primitive.u64.html'>u64</a></code></h4>
<div class='docblock'><p>Number of bytes of data written, including those handled by previous encryptors.</p>

<p>E.g. if this encryptor was constructed with a <code>DataMap</code> whose <code>len()</code> yields 100, and it
then handles a <code>write()</code> of 100 bytes, <code>len()</code> will return 200.</p>
</div><h4 id='method.is_empty' class='method'><code>fn <a href='#method.is_empty' class='fnname'>is_empty</a>(&amp;self) -&gt; <a class='primitive' href='https://doc.rust-lang.org/nightly/std/primitive.bool.html'>bool</a></code></h4>
<div class='docblock'><p>Returns true if <code>len() == 0</code>.</p>
</div></div></section>
    <section id='search' class="content hidden"></section>

    <section class="footer"></section>

    <aside id="help" class="hidden">
        <div>
            <h1 class="hidden">Help</h1>

            <div class="shortcuts">
                <h2>Keyboard Shortcuts</h2>

                <dl>
                    <dt>?</dt>
                    <dd>Show this help dialog</dd>
                    <dt>S</dt>
                    <dd>Focus the search field</dd>
                    <dt>&larrb;</dt>
                    <dd>Move up in search results</dd>
                    <dt>&rarrb;</dt>
                    <dd>Move down in search results</dd>
                    <dt>&#9166;</dt>
                    <dd>Go to active search result</dd>
                    <dt>+</dt>
                    <dd>Collapse/expand all sections</dd>
                </dl>
            </div>

            <div class="infos">
                <h2>Search Tricks</h2>

                <p>
                    Prefix searches with a type followed by a colon (e.g.
                    <code>fn:</code>) to restrict the search to a given type.
                </p>

                <p>
                    Accepted types are: <code>fn</code>, <code>mod</code>,
                    <code>struct</code>, <code>enum</code>,
                    <code>trait</code>, <code>type</code>, <code>macro</code>,
                    and <code>const</code>.
                </p>

                <p>
                    Search functions by type signature (e.g.
                    <code>vec -> usize</code> or <code>* -> vec</code>)
                </p>
            </div>
        </div>
    </aside>

    

    <script>
        window.rootPath = "../";
        window.currentCrate = "self_encryption";
        window.playgroundUrl = "";
    </script>
    <script src="../jquery.js"></script>
    <script src="../main.js"></script>
    
    <script defer src="../search-index.js"></script>
</body>
</html>